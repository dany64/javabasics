package com.memory_management;

public interface CustomerReadOnly {
    String getName();

    @Override
    String toString();
}
