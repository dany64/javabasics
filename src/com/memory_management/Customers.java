package com.memory_management;

import java.util.*;
import java.util.function.Consumer;

public class Customers {

    public static void main(String[] args) {
        CustomerRecords records = new CustomerRecords();
        records.addCustomer(new Customer("Toto"));
        records.addCustomer(new Customer("Simon"));

        for (Customer next: records){
            System.out.println(next);
        }

        Customer john = records.getCustomerByName("Toto");
        System.out.println(john);
        john.setName("Derek");
        System.out.println(john);

        for (Customer next: records.getCustomers().values()){
            System.out.println(next);
        }

        Customer c = new Customer("toto");
        System.out.println(c.getTotoName());
    }
}

class CustomerRecords implements Iterable<Customer>{
    private Map<String, Customer> records;

    public CustomerRecords(){
        this.records = new HashMap<String, Customer>();
    }

    public void addCustomer(Customer c){
        this.records.put(c.getName(),c);
    }

    public Customer getCustomerByName(String name){
        return new Customer(records.get(name));
    }

    /**
     * Deuxième méthode de getter qui retourne cette fois-ci la reférence à une nouvelle map qui est la copie de
     * la propriété records.
     * Cette facon de faire est meilleure que la première. Malgré tout, elle retourne une copie de la map qui peut
     * être modifiée comme on le souhaite.
     * @return The reference of a new Map which is the copy of the Map records.
     */
    public Map<String, Customer> getCustomers() {
        return Collections.unmodifiableMap(this.records);
    }

    /**
     * Deuxième méthode de getter qui retourne cette fois-ci la reférence à une nouvelle map qui est la copie de
     * la propriété records.
     * Cette facon de faire est meilleure que la première. Malgré tout, elle retourne une copie de la map qui peut
     * être modifiée comme on le souhaite.
     * @return The reference of a new Map which is the copy of the Map records.
     */
    public Map<String, Customer> getCustomers2() {
        return new HashMap<String, Customer>(this.records);
    }

    /**
     * Première méthode de getter qui se contente de retourner la Map (Plus précisément la reférence sur celle-ci).
     * Cette facon de faire n'est pas bonne, car elle expose une faille: celle de retourner la référence de la map
     * avec laquelle on peut désormais modifier l'attribut interne.
     * @Exemple:  Map<String, Customer> customers = cusotmerRecords.getCustomers3(); customers.clear;
     * Dans cet exemple, l'attribut records de l'objet cusotmerRecords est désormais vide, car la méthode
     * getCustomer3() a permit d'obtenir la reférence de cette Map et donc de pour voir la manipuler à notre guise
     * @return The reference of the Map records.
     */
    public Map<String, Customer> getCustomers3() {
        return this.records;
    }

    @Override
    public Iterator<Customer> iterator() {
        return records.values().iterator();
    }

}

class Customer implements CustomerReadOnly {
    private String name;

    @Override
    public String getName() {
        return name;
    }

    public Customer(String name) {
        Toto toto = new Toto();
        toto.name = name;
        this.name = name;
    }

    public Customer(Customer customer){
        this.name = customer.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }

    public String getTotoName(){
        return new Toto().name;
    }

    private class Toto{
        String name;

        public Toto(){
            name = "toto";
        }
        public String getName() {
            return name;
        }
    }
}
