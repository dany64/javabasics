package com.assertions;

import com.test.A;

public class Main {

    public static void main(String[] args) {
        int x = 10 + 15;
        assert x == 10: "x = " + x;
        int z = x +  12;

        byte b = 8;
        char c = 16;
        char c1 = (char) (b + c);
        //System.out.println("Je suis dans Main class");
        //A.main("args");
    }
}

