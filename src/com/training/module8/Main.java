package com.training.module8;


public class Main {

    public static void main(String[] args) {
        //Test construction
        A a;
       //a = new A();
        a = new A();
        A b;
        b = new B();

        a.present();
        a.show();
        ((A)b).present();
        ((A)b).show();

        A b1 = new B(5);
        b1.show();
        b1.present();

        B b2 = new B(6);
        A a1 = b2;
        b2.show();
        b2.present();
        a1.show();
        a1.present();

        final B b3 = new B(6);
        B b4 = b3;
        b4.b = 8;
        b3.show();

    }
}

class A{

//    {
//        System.out.println("Inside instance initializer");
//    }
//   static  {
//        System.out.println("bloc declatation A");
//        //show();
//        A a  = new A();
//        a.show();
//    }

    public A(){
        System.out.println("Construction of A");
    }

    public A(int i){
        System.out.println("Construction of A int");
    }

    public void show(){
        System.out.println("Hello A");
    }

    public void present(){
        System.out.println(this.getClass().getName());
    }
}

class B extends A{

    int b;

    public B(){
        System.out.println("Construction of B");
    }

    public B(int i){
        super(i);
        System.out.println("Construction of B int");
        this.b = i;
    }

    public void show(){
        System.out.println("Hello B with value "+b);
    }
}

