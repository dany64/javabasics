package com.training.module8;

/*
 We can create object by:
 - Invention
 - Discovery
 */

/*
Types of interfaces:
- Marker Interface -> without any methods
- SAM -> Single Abstract Method -> Functional Interface
- Normal Interface
 */

import java.io.Serializable;

public class Interface implements Serializable, Cloneable{

    public static void main(String[] args) {
        C c = new C() {
            @Override
            public void show() {
                System.out.println("Hello");
            }
        };
        c.show();

        C a = () -> System.out.println("Hello");

        Demo d = new Demo();
        if( d instanceof P)
            d.show();
        else
            System.out.println("No permission");

        Native n = new Native();
        n.m();
    }
}

interface C{
    public void show();
}

class Demo implements P{
    void show(){
        System.out.println("hello");
    }
}
interface P{

}

class Native {
    static{
        System.loadLibrary("Native library path");
    }
    public native void m();
}


