package com.toto;

public class Test {
    private int _;

    public int get_() {
        return _;
    }

    public static void main(String[] args) {
        int _ = 2;
        toto:;
        int __ = 3;
        int ___ = _ + __;

        System.out.println("_ + __ = " + ___);
        System.out.println("test test test");

        int num1 = 021;
        System.out.println("num1 = " + num1);
        
        int num2 = 0x0F;
        System.out.println("num2 = " + num2);
        
        int num3 = 0b11011;
        System.out.println("num3 = " + num3);

        int sum = num1 + num2 + num3;
        System.out.println("sum = " + sum);
        System.out.println("sum = " + Integer.toHexString(sum));
        System.out.println("sum = " + Integer.toBinaryString(sum));
        System.out.println("sum = " + Integer.toOctalString(sum));
        System.out.println("sum = " + Integer.toUnsignedString(sum));
        System.out.println("sum = " + Integer.toUnsignedString(-5000));

        long x = 999_999_999_999_999_999L;


        byte b = (byte)150;
        System.out.println("b = " + b);

    }
}
