package com.training.module10;

import java.io.*;

public class Main {

    public static void main(String[] args) throws IOException {

        int i, j, k;

        i = 8;
        j = 0;

        try {
            k = i/j;
        }
        catch (ArithmeticException e) {
          //  e.printStackTrace();
            System.out.println("Erreur: Division par zéro impossible");
        }
        File file = new File("/src/com/training/module10/toto.txt");
        if (file.exists())
            System.out.println("Le fichier existe");
        System.out.println("file.getAbsolutePath() = " + file.getAbsolutePath());
        System.out.println("file.getName() = " + file.getName());
        System.out.println("file.canExecute() = " + file.canExecute());
        System.out.println("file.canRead() = " + file.canRead());
        System.out.println("file.canWrite() = " + file.canWrite());
        System.out.println("file.getParent() = " + file.getParent());
        System.out.println("file.isFile() = " + file.isFile());
        System.out.println("file.isAbsolute() = " + file.isAbsolute());
        System.out.println("file.isDirectory() = " + file.isDirectory());
        System.out.println("file.isHidden() = " + file.isHidden());


        InputStream in;
        try (BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(new File("/src/com/training/module10/toto.txt"))))) {
           // String line = br.readLine();
           // System.out.println(line);
        } catch (IOException e) {
            System.out.println("Fichier spécifié introuvable");
        }




    }

    class MyException extends Exception{

        public MyException(String message){
            super(message);
        }
    }
}
