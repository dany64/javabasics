package com.training.module5;

import org.jetbrains.annotations.Contract;

public class Main {

    public static void main(String[] args) {
        if (System.out.printf("Hello World\n") == null) {

        }

//        i:
//        for (int i = 0; i < 4; i++) {
//            j:
//            for (int j = 0; j < 4; j++) {
//                k:
//                for (int k = 0; k < 4; k++) {
//                    if (i==2)
//                        break i;
//                    System.out.print("* ");
//                }
//                System.out.print("\t");
//            }
//            System.out.println("");
//        }

//        for (int i = 0; i < 128; i++) {
//            System.out.println(((int) i)+" = " + (char)i);
//            System.out.printf("%d = %c\n", i, i);
//        }
        printCircleMatrix(3);
        printCircleMatrix(4);
        printCircleMatrix(5);
        printCircleMatrix(6);
        printCircleMatrix(7);
        printCircleMatrix(8);
        printCircleMatrix(9);
        printCircleMatrix(10);

        printTriangleBinaryMatrix(5);
        printTriangleSameValueForLineMatrix(5);

        printFibonacciSerie(10);
        System.out.println(isPalindromeNumber(5));
        System.out.println(isPalindromeNumber(515));
        System.out.println(isPalindromeNumber(515515));
        System.out.println(isPalindromeNumber(121));
        System.out.println(isPalindromeNumber(1213));
        System.out.println();

        System.out.println(isPerfectNumber(6));
        System.out.println(isPerfectNumber(10));
        System.out.println(isPerfectNumber(23));
        System.out.println(isPerfectNumber(24));
        System.out.println(isPerfectNumber(28));
        System.out.println();

        System.out.println(isAmstrongNumber(153));
        System.out.println(isAmstrongNumber(125));
        System.out.println(isAmstrongNumber(370));
        System.out.println(isAmstrongNumber(1634));
        System.out.println();

        System.out.println(isPrimeNumber(2));
        System.out.println(isPrimeNumber(17));
        System.out.println(isPrimeNumber(25));
        System.out.println(isPrimeNumber(37));
        System.out.println(isPrimeNumber(35));

        System.out.println();
        System.out.println();

        //different ways to swap two integers
        int a = 5, b = 6;

        System.out.println("Values to swap: "+a +" "+b);

        //First: Using an intermediate variable
        int temp = a;
        a = b;
        b = temp;
        System.out.println("Values swapped: "+a +" "+b);

        //Second: without use of intermediate variable
        a = a + b;
        b = a - b; // ie b = a + b -b -> a, since a = a+b
        a = a - b; // ie a = a+b - a -> b
        System.out.println("Values swapped without temp variable: "+a +" "+b);

        //Third: Using of XOR operator in oder to save memory space used for the operation
        // a = 5 et b = 6
        a = a ^ b; //  1 0 1
                    // 1 1 0
                    // 0 1 1
        b = a ^ b; //  0 1 1
                    // 1 1 0
                    // 1 0 1
        a = a ^ b; //  0 1 1
                    // 1 0 1
                    // 1 1 0
        System.out.println("Values swapped without temp variable an using XOR: "+a +" "+b);

        //Fourth: Using the assign operator
        b = a + b - (a=b); // The  value of

        System.out.println("Values swapped without temp variable and using assignment: "+a +" "+b);
    }

    public static void printCircleMatrix(int length){
        int val;
        for (int i = 1; i <= length; i++) {
            for (int j = i; j < i+length; j++) {
                val = j%length==0?length:j%length;
                System.out.print(val+" ");
            }
            System.out.println();
        }
        System.out.println();
    }

    public static void printTriangleBinaryMatrix(int length){
        int val;
        for (int i = 1; i <= length; i++) {
            for (int j = 1; j <= i; j++) {
                val = (i+j)%2==0?1:0;
                System.out.print(val+" ");
            }
            System.out.println();
        }
        System.out.println();
    }

    public static void printTriangleSameValueForLineMatrix(int length){
        int val;
        for (int i = 1; i <= length; i++) {
            for (int j = 1; j <= i; j++) {
                System.out.print(i+" ");
            }
            System.out.println();
        }
        System.out.println();
    }

    /**
     * Print the #n first values of Fibonacci serie
     * @param n
     */
    public static void printFibonacciSerie(int n){
        int val =0,val1=0,val2=0;
        int i=0;
        if (n <=2) {
            System.out.println(val);
        }
        else{
            val1 = 1;
            val2 = 0;
            System.out.print("1 ");
            while(i <=n) {
                val = val1+ val2;
                System.out.print(val+" ");
                val2 = val1;
                val1 = val;
                i++;
            }
        }

    }

    /**
     * Reverse a given integer. It place its digits in the reverse order
     * @param n
     * @return
     */
    public static int reverseNumber(int n){
        int s = 0, r=0;
        while (n>0) {
            r = n % 10;
            n = n / 10;
            s = s * 10 + r;
        }
        return s;
    }

    /**
     * Test if a given number is a palindrome number
     * @param n
     * @return
     */
    public static boolean isPalindromeNumber(int n){
        return n == reverseNumber(n);
    }

    /**
     * Test if a first number is a divisor of the second one
     * @param n
     * @return
     */
    public static boolean isDivisor(int d, int n){
        return n % d == 0;
    }

    /**
     * Test if a given number is a perfect number. So that it is equal to the sum of its divisors
     * @param n
     * @return
     */
    @Contract(pure = true)
    public static boolean isPerfectNumber(int n){
        int sum = 0;
        for (int i = 1; i < n; i++) {
            if (isDivisor(i,n))
                sum += i;
        }
        return  sum == n;
    }

    /**
     * Test if a given number is an Amstrong number.
     * So that it is equal to the sum of its digits each of them raised to the power of the length of the number.
     * for example: 1634 = 1^4 + 6^4 + 3^4 + 4^4
     *              153 = 1^3 + 5^3 + 3^3
     * @param n
     * @return
     */
    public static boolean isAmstrongNumber(int n){
        int size = sizeOfInteger(n);
        int t = n;
        int sum = 0, r=0;
        while (n>0) {
            r = n % 10;
            n = n / 10;
            sum += Math.pow(r,size);
        }
        return sum == t;
    }

    /**
     * Return the length or a given integer in terms of number of digits
     * @param n
     * @return
     */
    public static int sizeOfInteger(int n){
        return (""+n).length();
    }


    /**
     * Test if a given number is a prime number or not
     * @param n
     * @return
     */
    public static boolean isPrimeNumber(int n){
        boolean isPrime = true;
        for (int i = 2; i < n; i++) {
            isPrime = !isDivisor(i,n);
            if (!isPrime)
                return false;
        }
        return isPrime;
    }
















    public static void myMethod(){
        Object obj1;
        Object obj2;

        if (true){
            obj1 = new Integer(10);
        }
        else {
            obj1 = new Double(15.0);
        }

        System.out.println(obj1);

        obj2 = true? 8: 4.0;
        System.out.println(obj2);
        System.out.println(obj2 instanceof Double);
    }
}
