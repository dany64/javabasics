package com.training.module9;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws IOException {
        int i;
        String line ="";
        while ((i = System.in.read()) != 10) {
            line += (char) i;
        }
        System.out.println(line);

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        System.out.println(br.readLine());

        Scanner scanner = new Scanner(System.in);
        System.out.println(scanner.nextLine());

    }
}
