package com.test;

public class TestPerson {

    public static void main(String[] args) {
        Address address = new Address("3216848","toto@gmail.com","Cameroun");
        Person p = new Person("toto", 18,address);
        System.out.println(p.toString());
        //Address address1 = new Address("3216848","ttiti@gmail.com","Gabon");
        Address address2 = p.getAddress();
        address2.setCountry("Gabon");

        System.out.println(p.toString());
    }
}

// Escaping reference