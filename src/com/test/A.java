package com.test;

import com.sun.xml.internal.stream.buffer.XMLStreamBuffer;

import java.io.BufferedInputStream;
import java.util.stream.Stream;

public class A{


    public static void main(String... args){
        for (String arg : args) {
            System.out.println(arg);
        }
        A a = new A();
        a.SumInc();
        a.SumPrePostInc();
        a.SumPostPreInc();
        a.SumPrePreInc();
        a.SumPostPostInc();

        String s = "toto";
         s = new String("titi");
         String s1 = new String("titi");
         String s2 = new String("titi");
         String t = "titi";
        //s = setString(s);
        System.out.println(s == t);
        System.out.println(s.equals(t));

        "toto".toString();
        String s3 = null;
        System.out.println("".equals(s3));
        char c = 65;
        c += 1 ;
        c++;
        System.out.println("c = " + c);

    }

    /**
     * met une chaine prise en paramètre en majuscule et retourne la
     * @param s
     * @return
     */
    public static String  setString(String s){
        return s = s.toUpperCase();
    }

    /**
     * Somme de post incrementation et d'une
     */
    public void SumInc(){
        int i = 10;
        i = i++ + i;
        System.out.println("i = " + i);
    }


    public void SumPrePostInc(){
        int i = 10;
        i = ++i + i++;
        System.out.println("i = " + i);
    }

    public void SumPostPreInc(){
        int i = 10;
        i = i++ + ++i;
        System.out.println("i = " + i);
    }

    public void SumPostPostInc(){
        int i = 10;
        i = i++ + i++;
        System.out.println("i = " + i);
    }

    public void SumPrePreInc(){
        int i = 10;
        i = ++i + ++i;
        System.out.println("i = " + i);
    }

}
