package com.test;

public class Main {
    public static void main(String[] args) {
        Region mid = new State();
        State md = new Maryland();
        Object obj = new Place();
        Place usa = new Region();
        md.printMe(); //Read it
        mid.printMe(); // Ship it
        ((Place) obj).printMe();// Buy it
        obj = md;
        ((Maryland) obj).printMe(); // Read it
        obj = usa;
        ((Place) obj).printMe();// box it
        usa = md;
        ((Place) usa).printMe(); // Read it
    }
}
class Maryland extends State {
    public void printMe() { super.printMe();System.out.println("Read it.");}
}
class State extends Region {
    public void printMe() { super.printMe(); System.out.println("Ship it."); }
}
class Region extends Place {
    public void printMe() { super.printMe();System.out.println("Box it."); }
}
class Place extends Object {
    public void printMe() { System.out.println("Buy it."); }
}